import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

const LinkButton = ({to, label}) => {
  // Можно лучше: в стрелочной функции можно обойтись без оператора return, вернув jsx в круглых скобках
  // например
  // const LinkButton = (...) => (
  // <Link to={to}>
  //  ...
  // );

  return (
    <Link to={to}>
      <button>{label}</button>
    </Link>
  )
}

LinkButton.propTypes = {
  to: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
}

export default LinkButton
