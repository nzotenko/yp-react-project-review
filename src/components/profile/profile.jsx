import React from 'react'
import PropTypes from 'prop-types'

const Profile = ({ user }) => {
  // Можно лучше: в стрелочной функции можно обойтись без оператора return, вернув jsx в круглых скобках
  return (
    <React.Fragment>
      <h2>Профиль</h2>
      <p>Вас зовут: {user.name}</p>
    </React.Fragment>
  )
}
// Надо исправить: свойство регистрозависимо, чтобы валидация работала, необходимо указать propTypes
// Можно лучше: Можно передать в профиль один строковый пропс с именем, так как из объекта используется только оно
Profile.proptypes = {
  user: PropTypes.shape({
    name: PropTypes.string.isRequired,
  }).isRequired,
}

export default Profile
