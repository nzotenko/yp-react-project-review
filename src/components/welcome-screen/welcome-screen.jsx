import React from 'react'

const WelcomeScreen = () => {
  // Можно лучше: в стрелочной функции можно обойтись без оператора return, вернув jsx в круглых скобках
  return (
      <h2>Главная страница</h2>
  )
}

export default WelcomeScreen
