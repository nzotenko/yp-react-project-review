import React, {PureComponent} from 'react'
import { connect } from 'react-redux'
import Profile from '../../components/profile/profile.jsx'

// Нужно исправить: ProfileContainer рендерит Profile и имеет такие же пропсы
// следовательно в дополнительном слое нет необходимости, в функцию connect можно передать сразу компонент Profile
class ProfileContainer extends PureComponent {
  render() {
    const { user } = this.props
    return <Profile user={user} />
  }
}

const mapStateToProps = state => ({
  user: state.session.user,
})

const mapDispatchToProps = dispatch => ({})

// Нужно исправить: не обязательно передавать функцию mapDispatchToProps
// можно передать в функцию connect только один параметр mapStateToProps,
// а функцию mapDispatchToProps удалить
export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer)
